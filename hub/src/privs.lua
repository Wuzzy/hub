local S = minetest.get_translator("hub")

minetest.register_privilege("hub_admin", {
  description = S("It allows you to use /hub")
})